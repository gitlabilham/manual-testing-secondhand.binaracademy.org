
## Manual Testing (Secondhand.binaracademy.org)

Welcome to the Manual Testing (Secondhand.binaracademy.org) project repository! This repository contains test artifacts for the manual testing of https://secondhand.binaracademy.org/ an online second hand web store. The repository includes test cases and a test execution report to effectively plan, execute, and track the testing activities for Secondhand.binaracademy.org


## Table of Contents

- Introduction
- Project Overview
- Test Artifacts
- Test Case

## Introduction
Manual testing plays a critical role in ensuring the quality and reliability of secondhand.binaracademy.org an online second hand web store. This repository serves as a centralized resource for storing and organizing test artifacts related to flow of the website. 
## Project Overview
Secondhand.binaracademy.org is a web application that offers an extensive and variety of second hand products. A platform designed to connect sellers and buyers in the secondhand market, provides a seamless experience for both sellers and buyers. In this testing phase, we meticulously testing the platform's functionality from start to finish, emphasizing key aspects of the features.
## Test Artifacts
The following test artifacts are included in this project:
- Test Cases
- Test Execution Report

## Test Case
In this project, test case  include the following information:
- Test case ID
- Test case description
- Pre-condition
- Test steps
- Expected results
- Actual results 
- Test Conditions (Positive/Negative)
- Status Test (Pass/Fail)
